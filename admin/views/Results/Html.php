<?php
/**
 * @package    JSport
 *
 * @author     smirnov_d <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

namespace Joomplace\Component\JSport\Administrator\views\Results;

defined('_JEXEC') or die;


use JoomPlaceX\Sidebar;
use JoomPlaceX\View;


class Html extends View
{
    public function render($tpl = null)
    {
        switch ($this->getLayout()){
            case 'edit':
                break;
            default:
                $this->sidebar = Sidebar::render();
        }
        return parent::render($tpl);
    }
}




