<?php
/**
 * @package    JSport
 *
 * @author     smirnov_d <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

defined('_JEXEC') or die;

JLoader::registerNamespace('Joomplace\Component\JSport\Administrator',JPATH_SITE . '/administrator/components/com_jsport',false,false,'psr4');
JLoader::registerNamespace('Joomplace\Component\JSport\Site',JPATH_SITE . '/components/com_jsport',false,false,'psr4');

// Access check.
//if (!JFactory::getUser()->authorise('core.manage', 'com_JSport'))
//{
//	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
//}

$input = JFactory::getApplication()->input;
$component = new \Joomplace\Component\JSport\Administrator\Dispatcher();
$component->dispatch($input->get('task'));



