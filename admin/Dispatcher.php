<?php

namespace Joomplace\Component\JSport\Administrator;

use JoomPlaceX\Sidebar;

class Dispatcher extends \JoomPlaceX\Dispatcher
{
    protected $namespace = __NAMESPACE__;
    protected $default_view = 'JSport';

    protected function addMustHaveButtons()
    {
        jimport('helpers.JSport',dirname(__FILE__));

        \JSportHelper::addSubmenu($this->input->get('view'));
        parent::addMustHaveButtons();
    }
}