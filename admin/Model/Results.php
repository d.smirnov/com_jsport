<?php

namespace Joomplace\Component\JSport\Administrator\Model;

class Results extends \JoomPlaceX\Model
{
    private $setsCount = 5;

    protected static $_fields = array(
        'published' => array(
            'mysql_type' => 'int(1) unsigned',
            'type' => 'radio',
            'label' => 'COM_FAQ_PUBLISHED_LABEL',
            'description' => 'COM_FAQ_PUBLISHED_LABEL_DESC',
            'class' => 'btn-group',
            'nullable' => false,
            'default' => 0,
            'option' => array(
                0 => 'JNO',
                1 => 'JYES',
            ),
            'hide_at' => array('list'),
        )
    );

    protected function conversion($shedule){
        $tempArray ['team1_id'] = $shedule[0];
        $tempArray ['team2_id'] = $shedule[1];
        foreach ($tempArray as $fieldName => $teamId){
            $allSheduleOfTheTeam['home'] = (new \Joomplace\Component\JSport\Administrator\Model\Shedule())->getList(null, null, array('team1_id' => $teamId, 'is_completed' => 1), \stdClass::class);
            $allSheduleOfTheTeam['guest'] = (new \Joomplace\Component\JSport\Administrator\Model\Shedule())->getList(null, null, array('team2_id' => $teamId, 'is_completed' => 1), \stdClass::class);
            //формируем массив результатов по сетам
            $allResultIdList = [];
            foreach ($allSheduleOfTheTeam as $location => $sheduleList){
                foreach ($sheduleList as $item) {
                    $allResultIdList[$location] = $item->id;
                }
            }

            // получаем все результаты всех игр данной команды
            $allResultsList = [];
            foreach ($allResultIdList as $location => $idList){
                $allResultsList[$location] = (new \Joomplace\Component\JSport\Administrator\Model\Results())->getList(null, null, array('game_id' => $allResultIdList[$location]), \stdClass::class);
            }

            $count = count($allResultsList['home'])+count($allResultsList['guest']);

            // пересчет
            $diff = [];
            $points = 0;
            foreach ($allResultsList as $location => $resultList){
                foreach ($resultList as $result){
                    $resultsList = $this->getResultsList($result);
                    $gameResult = $this->getGameResult($resultsList);
                    $gamePoints = $this->getGamePoints($gameResult);

                    if($location == 'home'){
                        $diff[0] += $gameResult[0];
                        $diff[1] += $gameResult[1];
                        $points += $gamePoints[0];
                    } else {
                        $diff[0] += $gameResult[1];
                        $diff[1] += $gameResult[0];
                        $points += $gamePoints[1];
                    }
                }
            };

            $modelTeam = (new \Joomplace\Component\JSport\Administrator\Model\JSport($teamId));

            $modelTeam->bind([
                'diff_sets' => $diff[0].':'.$diff[1],
                'total_points' =>$points,
                'games_count' => $count,
            ]);
            $modelTeam->store();
        };
        /*$allSheduleOfTheTeam1 = (new \Joomplace\Component\JSport\Administrator\Model\Shedule())->getList(null, null, array('team1_id' => $shedule->team1_id), \stdClass::class);
        $allSheduleOfTheTeam2 = (new \Joomplace\Component\JSport\Administrator\Model\Shedule())->getList(null, null, array('team2_id' => $shedule->team2_id), \stdClass::class);

        //формируем массив результатов по сетам team 1
        $allResultIdList1 = [];
        foreach ($allSheduleOfTheTeam1 as $item){
//print_r($item);
            array_push($allResultIdList1,$item->id);
        }

        // получаем все результаты всех игр данной команды
        $allResultsList1 = (new \Joomplace\Component\JSport\Administrator\Model\Results())->getList(null, null, array('game_id' => $allResultIdList1), \stdClass::class);

        //формируем массив результатов по сетам team 2
        $allResultIdList2 = [];
        foreach ($allSheduleOfTheTeam2 as $item){
            array_push($allResultIdList2,$item->id);
        }

        // получаем все результаты всех игр данной команды
        $allResultsList2 = (new \Joomplace\Component\JSport\Administrator\Model\Results())->getList(null, null, array('game_id' => $allResultIdList2), \stdClass::class);

        $count1 = count($allResultsList1);
        $count2 = count($allResultsList2);

        // пересчет t1
        $diff1 = [];
        $points1 = 0;
        foreach ($allResultsList1 as $item){
            $resultsList1 = $this->getResultsList($item);
            $gameResult1 = $this->getGameResult($resultsList1);
            $diff1[0] += $gameResult1[0];
            $diff1[1] += $gameResult1[1];
            $gamePoints1 = $this->getGamePoints($gameResult1);
            $points1 += $gamePoints1[0];
        }

        // пересчет t2
        $diff2 = [];
        $points2 = 0;
        foreach ($allResultsList2 as $item){
            $resultsList2 = $this->getResultsList($item);
            $gameResult2 = $this->getGameResult($resultsList2);
            $diff2[0] += $gameResult2[0];
            $diff2[1] += $gameResult2[1];
            $gamePoints2 = $this->getGamePoints($gameResult2);
            $points2 += $gamePoints2[0];
        }


        // запись расчетов в команду
        $a = (new \Joomplace\Component\JSport\Administrator\Model\JSport($shedule->team1_id));

        $a->bind([
            'diff_sets' => $diff1[0].':'.$diff1[1],
            'total_points' =>$points1,
            'games_count' => $count1,
        ]);
        $a->store();

        $b = (new \Joomplace\Component\JSport\Administrator\Model\JSport($shedule->team2_id));


        $b->bind([
            'diff_sets' => $diff2[1].':'.$diff2[0],
            'total_points' =>3-$points2,//3-очки t1
            'games_count' => $count2,
        ]);
        $b->store();*/

        return true;
    }

    public function store($updateNulls = false)
    {

        //получаем текущую игру
        $shedule = (new \Joomplace\Component\JSport\Administrator\Model\Shedule($this->game_id));
        $shedule->is_completed = 1;
        $shedule->store();

        parent::store($updateNulls);

        //получаем текущую игру
        //$shedule = (new \Joomplace\Component\JSport\Administrator\Model\Shedule($this->game_id));

        // test conversion
        $this->conversion(array($shedule->team1_id, $shedule->team2_id));
        //$this->conversion($shedule);

        return true;
    }

    protected function determine()
    {
        $this->_table = '#__jsport_results';
        $this->_context = 'com_jsport.results';
    }

    protected function getResultsList($result){
        $res_array = [];
        for($i=1; $i <= $this->setsCount; $i++){
            $field1Name = 'set'.$i.'_t1';
            $field2Name = 'set'.$i.'_t2';
            $res_array[0][$i-1] = $result->$field1Name;
            $res_array[1][$i-1] =$result->$field2Name;
        }
        return $res_array;
    }

    protected function getGameResult($resultsList){
        $team1 = 0;
        $team2 = 0;
        for ($i=0; $i < $this->setsCount; $i++){
            if($resultsList[0][$i] !='' || $resultsList[1][$i] !='')
                ($resultsList[0][$i] > $resultsList[1][$i]) ? $team1++ : $team2++;
        }
        return array($team1, $team2);
    }

    protected function getGamePoints($gameResult){
        $res_array= [];

        $diff = $gameResult[0] - $gameResult[1];

        if($diff >= 2){
            $res_array[0] =  3;
            $res_array[1] =  0;
        } elseif ($diff == 1){
            $res_array[0] =  2;
            $res_array[1] =  1;
        } elseif ($diff == -1){
            $res_array[0] =  1;
            $res_array[1] =  2;
        } elseif ($diff <= -2){
            $res_array[0] =  0;
            $res_array[1] =  3;
        }
        return $res_array;
    }
}