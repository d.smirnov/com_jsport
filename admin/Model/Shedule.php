<?php

namespace Joomplace\Component\JSport\Administrator\Model;


class Shedule extends \JoomPlaceX\Model
{
    protected static $_fields = array(
        'published' => array(
            'mysql_type' => 'int(1) unsigned',
            'type' => 'radio',
            'label' => 'COM_FAQ_PUBLISHED_LABEL',
            'description' => 'COM_FAQ_PUBLISHED_LABEL_DESC',
            'class' => 'btn-group',
            'nullable' => false,
            'default' => 0,
            'option' => array(
                0 => 'JNO',
                1 => 'JYES',
            ),
            'hide_at' => array('list'),
        ),
        'is_completed' => array(
            'mysql_type' => 'int(1) unsigned',
            'type' => 'radio',
            'label' => 'COM_JSPORT_IS_COMPLETED_LABEL',
            'description' => 'COM_FAQ_PUBLISHED_LABEL_DESC',
            'class' => 'btn-group',
            'nullable' => false,
            'default' => 0,
            'option' => array(
                0 => 'JNO',
                1 => 'JYES',
            ),
            'hide_at' => array('list'),
        )
    );

    public function store($updateNulls = false)
    {
        if ($this->alias == null) {
            $this->set('alias', \JFilterOutput::stringURLSafe($this->name));
        }



        return parent::store($updateNulls);
    }

    protected function determine()
    {
        $this->_table = '#__jsport_shedule';
        $this->_context = 'com_jsport.shedule';
    }

    public function getCountGames($result){
//        $db = JFactory::getDbo();
//
//        $query = $db->getQuery(true)
//            ->select('m.id, m.title, m.module, m.position, m.showtitle, m.params')
//            ->from('#__modules AS m')
//            ->where('m.module =' . $db->quote($module) . ' AND m.client_id = 1')
//            ->join('LEFT', '#__extensions AS e ON e.element = m.module AND e.client_id = m.client_id')
//            ->where('e.enabled = 1');
    }

}