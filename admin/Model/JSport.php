<?php

namespace Joomplace\Component\JSport\Administrator\Model;


class JSport extends \JoomPlaceX\Model
{
    public function __construct($conditions = null, $reset = true)
    {
        parent::__construct($conditions, $reset);

    }

    protected static $_fields = array(
        'published' => array(
            'mysql_type' => 'int(1) unsigned',
            'type' => 'radio',
            'label' => 'COM_FAQ_PUBLISHED_LABEL',
            'description' => 'COM_FAQ_PUBLISHED_LABEL_DESC',
            'class' => 'btn-group',
            'nullable' => false,
            'default' => 0,
            'option' => array(
                0 => 'JNO',
                1 => 'JYES',
            ),
            'hide_at' => array('list'),
        )
    );

    public function store($updateNulls = false)
    {
        parent::store($updateNulls);

        if ($this->alias == null) {
            $this->set('alias', \JFilterOutput::stringURLSafe($this->name));
        }

        return true;


    }

    protected function determine()
    {
        $this->_table = '#__jsport_team';
        $this->_context = 'com_jsport.team';
    }

}