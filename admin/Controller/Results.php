<?php
/**
 * @package    JSport
 *
 * @author     smirnov_d <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

namespace Joomplace\Component\JSport\Administrator\Controller;

class Results extends \JoomPlaceX\Controller
{
    protected $_default_action = 'index';
}