<?php

echo JHtml::_('bootstrap.addTab', $displayData->set, $displayData->name, JText::_(($displayData->label) ? $displayData->label : ('FIELDSET_' . strtoupper($displayData->name))));

?>
<div class="form-horizontal">
    <div class="row-fluid">
        <div class="span9">
            <fieldset class="adminform">
                <?php echo $displayData->fields['jform_game_id']->renderField(); ?>
            </fieldset>
        </div>
        <div class="span3">
            <?php
            for ($i=1; $i<=5; $i++){
                echo $displayData->fields['jform_set'.$i.'_t1']->renderField();
                echo $displayData->fields['jform_set'.$i.'_t2']->renderField();
            }
            ?>
        </div>
    </div>
</div>
<?php
echo $displayData->fields['jform_id']->renderField();
echo $displayData->fields['jform_ordering']->renderField();
echo JHtml::_('bootstrap.endTab');
?>

