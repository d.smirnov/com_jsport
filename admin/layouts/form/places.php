<?php

echo JHtml::_('bootstrap.addTab', $displayData->set, $displayData->name, JText::_(($displayData->label) ? $displayData->label : ('FIELDSET_' . strtoupper($displayData->name))));

?>
<div class="form-inline form-inline-header">
    <div class="control-group">
        <?php echo $displayData->fields['jform_place_name']->renderField(); ?>
    </div>
    <!--<div class="control-group">
        <?php /*echo $displayData->fields['jform_alias']->renderField(); */?>
    </div>-->
</div>

<div class="form-horizontal">
    <div class="row-fluid">
        <div class="span9">
            <fieldset class="adminform">
                <?php //echo $displayData->fields['jform_note']->renderField(); ?>
            </fieldset>
        </div>
        <div class="span3">
            <?php
            echo $displayData->fields['jform_published']->renderField();
            //echo $displayData->fields['jform_catid']->renderField();
            ?>
        </div>
    </div>
</div>
<?php
echo $displayData->fields['jform_id']->renderField();
echo $displayData->fields['jform_ordering']->renderField();
echo JHtml::_('bootstrap.endTab');
?>

