<?php
/**
 * @package    JSport
 *
 * @author     smirnov_d <your@email.com>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       http://your.url.com
 */

defined('_JEXEC') or die;

/**
 * JSport helper.
 *
 * @package     A package name
 * @since       1.0
 */
class JSportHelper
{
	/**
	 * Render submenu.
	 *
	 * @param   string  $vName  The name of the current view.
	 *
	 * @return  void.
	 *
	 * @since   1.0
	 */
	public function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(
		    JText::_('COM_JSPORT_TEAMS'),
            'index.php?option=com_JSport&view=jsport',
            $vName == 'JSport'
        );

        JHtmlSidebar::addEntry(
            JText::_('COM_JSPORT_DIVISIONS'),
            'index.php?option=com_categories&extension=com_jsport',
            $vName == 'categories.jsport'
        );
		JHtmlSidebar::addEntry(
		    JText::_('COM_JSPORT_PLACES'),
            'index.php?option=com_JSport&view=places',
            $vName == 'places'
        );
		JHtmlSidebar::addEntry(
		    JText::_('COM_JSPORT_SHEDULE'),
            'index.php?option=com_JSport&view=shedule',
            $vName == 'shedule'
        );
		JHtmlSidebar::addEntry(
		    JText::_('COM_JSPORT_RESULTS'),
            'index.php?option=com_JSport&view=results',
            $vName == 'results'
        );
	}
}