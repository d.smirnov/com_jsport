<?php

namespace Joomplace\Component\JSport\Site;

class Dispatcher extends \JoomPlaceX\Dispatcher
{
    protected $namespace = __NAMESPACE__;
    protected $default_view = 'JSport';

    public function dispatch($task = null)
    {
        parent::dispatch($task);
    }

}