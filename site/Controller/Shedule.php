<?php

namespace Joomplace\Component\JSport\Site\Controller;

use Joomla\Image\ImageFilter;
use JoomPlaceX\Controller;

class Shedule extends Controller
{
    public function take()
    {
        $db = \JFactory::getDbo();
        $game_list = $db->getQuery(true);
//        $game_list = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\Shedule::class)->getList(null, null, array('published' => 1), \stdClass::class);
        $game_list = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\Shedule::class)->getListQuery();
        $game_list->clear('select')
            ->select('a.game_date, a.game_time, t.name, t2.name AS t2_name, p.place_name, c.title')
            ->clear('order')
            ->order('game_date DESC')
            ->order('place_id ASC')
            ->order('game_time ASC')
            ->join('left', $game_list->qn('#__jsport_team','t').' ON a.team1_id = t.id')
            ->join('left', $game_list->qn('#__jsport_team','t2').' ON a.team2_id = t2.id')
            ->join('left', $game_list->qn('#__jsport_places','p').' ON a.place_id = p.id')
            ->join('left', $game_list->qn('#__categories','c').' ON c.extension = "com_jsport" AND a.division_id = c.id');

//        $db = \JFactory::getDbo();
//        $game_list = $db->getQuery(true);
//        $game_list = "SELECT a.game_date, a.game_time, t.name, t2.name AS t2_name, p.place_name, c.title
//                      FROM `prfx_jsport_shedule` s
//                      JOIN `prfx_jsport_team` t ON s.team1_id = t.id
//                      JOIN `prfx_jsport_team` t2 ON s.team2_id = t2.id
//                      JOIN `prfx_jsport_places` p ON s.place_id = p.id
//                      JOIN `prfx_categories` c ON c.extension = 'com_jsport' AND s.division_id = c.id
//                      WHERE s.published = 1
//                      ORDER BY `game_date` DESC, `place_id` ASC, `game_time` ASC";

        $db->setQuery($game_list);

        $row = $db->loadObjectList();
//        echo "<pre>";
//        print_r($row);
//        die();
//        $this->db->setQuery($game_list);
//        $rows = $this->db->loadAssocList();
//        echo "<pre>";
//        print_r($rows);
//        die();

        //$results_list = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\Results::class)->getList(null, null, array('published' => 1), \stdClass::class, 'game_id');

        $vars = array(
            'game_list' => $row,//$game_list
        );

        $layout = $this->input->get('layout', 'default');
        $this->display('Shedule.' . $layout, $vars);

        return true;
    }

    public function showShedule()
    {
//        $slug = $this->input->get('id');
//        $layout = $this->input->get('layout', 'one');
//
//        $faq = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\JSport::class)->getList(null, null, array('alias' => $slug));
//
//        $this->display('JSport.' . $layout, array('faq' => $faq));
    }
}