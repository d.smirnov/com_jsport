<?php

namespace Joomplace\Component\JSport\Site\Controller;

use JoomPlaceX\Controller;

class JSport extends Controller
{
    public function take()
    {
//        $faqs = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\JSport::class)->getList(null, null, array('published' => 1));
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);

        $query = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\JSport::class)->getListQuery();
        $query->clear('where');
        $query->where(array('published' => 1));
        $query->clear('order');
        $query->order('total_points DESC');
        // add other rule for sort listOfTeams

        $db->setQuery($query);

        $listOfTeams = $db->loadObjectList();

        //$resultsModel = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\Results::class);

        //$results_list = $resultsModel->getList(null, null, array('published' => 1), \stdClass::class, 'game_id');

        //$resultsModel->getResult($results_list);

        $vars = array(
            'faqs' => $listOfTeams,
            //'results_list' => $results_list,
        );

        $layout = $this->input->get('layout', 'faq');
        $this->display('JSport.' . $layout, $vars);

        return true;
    }

    public function showFaq()
    {
        $slug = $this->input->get('id');
        $layout = $this->input->get('layout', 'one');

        $faq = $this->getModel(\Joomplace\Component\JSport\Administrator\Model\JSport::class)->getList(null, null, array('alias' => $slug));

        $this->display('JSport.' . $layout, array('faq' => $faq));
    }
}