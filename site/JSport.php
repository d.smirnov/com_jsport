<?php

defined('_JEXEC') or die('Restricted access');

JLoader::registerNamespace('Joomplace\Component\JSport\Administrator',JPATH_SITE.'/administrator/components/com_jsport',false,false,'psr4');
JLoader::registerNamespace('Joomplace\Component\JSport\Site',JPATH_SITE.'/components/com_jsport',false,false,'psr4');
$input = JFactory::getApplication()->input;
$component = new \Joomplace\Component\JSport\Site\Dispatcher();
$component->dispatch($input->get('task'));
