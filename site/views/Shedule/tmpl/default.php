<?php

defined('_JEXEC') or die('Restricted Access');

/** @var \Joomplace\Component\JSport\Administrator\Model\JSport */

$game_list = $this->game_list;

?>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>game_date</th>
        <th>division_id</th>
        <th>team_1</th>
        <th>team_2</th>
        <th>place_id</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($game_list as $key=>$item){
        echo "<tr>";
        echo "<td>$item->game_date : $item->game_time</td>";
        echo "<td>$item->title</td>";
        echo "<td>$item->name</td>";
        echo "<td>$item->t2_name</td>";
        echo "<td>$item->place_name</td>";
        echo "</tr>";
    }
    ?>
    </tbody>
</table>