<?php

defined('_JEXEC') or die('Restricted Access');

/** @var \Joomplace\Component\JSport\Administrator\Model\JSport */
$faqs = $this->faqs;

$categories = array();
foreach ($faqs as $key => $faq) {
    $categories[$faq->catid][] = $faq;
}

$cat = JTable::getInstance('Category');

$ordArr = [];

foreach ($categories as $index => $category){
    $cat->load($index);
    $ordArr[$index] = $cat->lft;
}
asort($ordArr);
//echo "<pre>";
//print_r($categories);
//die();
foreach ($ordArr as $index => $value){
    $ordArr[$index] = $categories[$index];
}
?>

<div class="accordion" id="accordion2">
    <?php
    foreach ($ordArr as $index => $category) { ?>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2"
                   href="#collapse<?php echo $index; ?>">
                    <?php
                    $cat->load($index);

                    echo $cat->title ? $cat->title : JText::_('NOCATEGORY');
                    ?>
                </a>
            </div>
            <div id="collapse<?php echo $index; ?>" class="accordion-body collapse">
                <div class="accordion-inner">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <th>#</th>
                        <th>Team name</th>
                        <th>Number of games</th>
                        <th>Sets</th>
                        <th>Points</th>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($categories[$index] as $key => $faq) { ?>
                            <tr class="list-group-item">
                                <!--<a href="<?php /*echo JRoute::_('index.php?option=com_JSport&view=one&id=' . $faq->id); */?>">
                                    <?php /*echo $faq->name; */?>
                                </a>-->
                                <td><?= $key+1;?></td>
                                <td><?= $faq->name;?></td>
                                <td><?= $faq->games_count;?></td>
                                <td><?= $faq->diff_sets ;?></td>
                                <td><?= $faq->total_points ;?></td>
                            </tr>
                        <?php }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
</div>